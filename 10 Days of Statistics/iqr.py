def median(arr):
    arr.sort()
    if len(arr)%2 == 0:
        temp = (arr[len(arr)//2] + arr[(len(arr)//2) - 1])/2
    else:
        temp = arr[len(arr)//2]
    return temp
def quartile(x,arr):
    arr.sort()
    
    q1 = median(arr[:x//2])
    if x%2 == 0:
        q3 = median(arr[x//2:])
    else:
        q3 = median(arr[x//2+1:])
    return int(q1),int(q3)

def main():
    x = int(input())
    arr = [float(item) for item in input().split()]
    arr2 = [int(item) for item in input().split()]
    list_f = []
    for i in range(x):
        for _ in range(arr2[i]):
            list_f.append(arr[i])
    q1, q3 = quartile(len(list_f),list_f)
    print(q3-q1)

if __name__ == '__main__':
    main()