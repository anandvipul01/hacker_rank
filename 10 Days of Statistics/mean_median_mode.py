# Enter your code here. Read input from STDIN. Print output to STDOUT



def mean(arr):
    return sum(arr)/len(arr)

def mode(arr):
    dict_arr = {}
    for item in arr:
        if item in dict_arr:
            dict_arr[item] += 1
        else:
            dict_arr[item] = 1
    max_key = max(dict_arr.keys(), key = lambda k: dict_arr[k])
    l = []
    for item in dict_arr:
        if dict_arr[item] == dict_arr[max_key]:
            l.append(item)
    l.sort()
    return l[0]

def median(arr):
    arr.sort()
    if len(arr)%2 == 0:
        temp = (arr[len(arr)//2] + arr[(len(arr)//2) - 1])/2
    else:
        temp = arr[len(arr)//2]
    return temp


num = int(input())
arr_num = input().split()
arr_num = [int(a) for a in arr_num]


print(mean(arr_num))
print(median(arr_num))
print(mode(arr_num))
