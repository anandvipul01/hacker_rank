def median(arr):
    arr.sort()
    if len(arr)%2 == 0:
        temp = (arr[len(arr)//2] + arr[(len(arr)//2) - 1])/2
    else:
        temp = arr[len(arr)//2]
    return temp
def quartile(x,arr):
    arr.sort()
    q2 = median(arr)
    q1 = median(arr[:x//2])
    if x%2 == 0:
        q3 = median(arr[x//2:])
    else:
        q3 = median(arr[x//2+1:])
    return int(q1),int(q2),int(q3)

def main():
    x = int(input())
    arr = [float(item) for item in input().split()]
    q1, q2, q3 = quartile(x,arr)
    print(q1)
    print(q2)
    print(q3)

if __name__ == '__main__':
    main()