def stdev(x,arr):
    mean = sum(arr)/x
    return (sum([(x - mean)**2 for x in arr])/x)**0.5
    
def main():
    x = int(input())
    arr = [float(item) for item in input().split()]
    print(round(stdev(x,arr),1))

if __name__ == '__main__':
    main()