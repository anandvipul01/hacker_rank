def w_mean(x,arr1,arr2):
    a = [arr1[i]*arr2[i] for i in range(x)]
    return sum(a)/sum(arr2)

def main():
    x = int(input())
    arr1 = input().split()
    arr2 = input().split()

    arr1 = [float(item) for item in arr1]
    arr2 = [float(item) for item in arr2]

    print(round(w_mean(x,arr1,arr2),1))

if __name__ == '__main__':
    main()